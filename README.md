
# Blossom Developer Guide

## About the Paid Trial

We'd rather just give you a chance to show us what you can do, 
rather than waste your time with coding interviews. This means we "hire fast" 
and let you show us your skills.

The Paid Trial is precisely that - a TRIAL. We're using this to evaluate your performance and 
potential ability to work with the team.

As part of the paid trial you will be assigned a small task. 
The goal of the paid trial is to complete that small task withing the time allotted. 

### Notes for Success

* Have a read through the "What You'll Need" section below to make a smooth start
* COMMUNICATE early and often. Problems? Let us know! 
* COMMUNICATE about your availability so we can manage our expectations accordingly
* COMMUNICATE by asking questions. Silence is bad, communication is good.
* Did we mention communicate?

## What You'll Need

### Dev Environment

* Linux or *nix compatible environment (Windows is not supported)
* Node.js installed
* MySQL
* MongoDB
* RabbitMQ

### Key Knowledge

* Node.js
* Angular 1
* REST API / HTTP
* Basic knowledge of BASH
* Git

## Project Management

* Features and changes live in [Trello](https://trello.com)
* Sometimes bugs live in Bitbucket issues, depending on the project

## Coding Practices

### Workflow 

* Use [git feature branch workflow](https://www.atlassian.com/git/tutorials/comparing-workflows)
* Open a unique branch named according to each Trello card or Bitbucket issue you are working on
* Push your work to the remote branch at the end of each working session
* Open a Pull Request in Bitbucket when the work is ready for review

### Code Quality

* Code should conform to both the  [JSHint](http://jshint.com/) and [JSBeautify](http://jsbeautifier.org/) specs in each project folder.
* Configure your preferred editor to use these
* Pull requests that do not pass both JSHint and JSBeautify will be rejected

